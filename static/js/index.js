/**
 *
 * str： 用于显示的消息
 *
 */
function showPopup(str) {
    var popupBox = document.getElementById("popupBox");
    if(popupBox == null || popupBox == undefined) {
        popupBox = document.createElement("div");
        popupBox.id = "popupBox";
        document.body.appendChild(popupBox);
    }

    popupBox.innerText = str;
    popupBox.style.opacity = "1";
    setTimeout(function() {
        popupBox.style.opacity = "0";
    }, 1000);
}